import React from 'react';
import { Provider } from 'react-redux';
import store from './redux/store';

import App from './App';
import {BrowserRouter} from "react-router-dom";

const Wrapper = () => {


  return (
    <BrowserRouter>
      <Provider store={store}>

            <App />

      </Provider>
    </BrowserRouter>
  );

}

export default Wrapper;
