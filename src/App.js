import React from 'react';
import './App.css';
import TodoList from "./components/TodoList";
import { connect } from 'react-redux';
import { Switch, Route, Link } from 'react-router-dom';


class App extends React.Component{

  state = {
    inputValue: '',
    itemId: 0
  };

  onInputChange = (e) => {
    this.setState({
      inputValue: e.target.value
    });
  };

  onInputSubmit = () => {
    const newListItem = this.state.inputValue;
    let itemId = this.state.itemId + 1;
    this.props.addToList( itemId, newListItem );
    this.setState({
      itemId: itemId
    })
  };

  render() {

    const { inputValue } = this.state;
    const { onInputChange, onInputSubmit } = this;

    return (
      <div className="App">
        <h1>Todo List</h1>
        <nav>
          <Link to="/">All Items</Link>
          <Link to="/done">Done Items</Link>
          <Link to="/undone">Undone Items</Link>
        </nav>
        <input
          value={inputValue}
          onChange={onInputChange}
        />
        <button onClick={onInputSubmit}>Add item</button>

        <Switch>
          <Route exact path="/" component={TodoList} />
          <Route path="/:slug" component={TodoList} />
          <Route path="/:slug" component={TodoList} />
        </Switch>
      </div>
    );

  }


}

/*
  Redux:
 */

const mapStateToProps = ( state ) => ({
  list: state.todoList
});

const mapDispatchToProps = ( dispatch ) => ({
  addToList: ( id, title ) => {
    dispatch({ type: 'ADD_LIST_ITEM', payload: { id, title, done: false }})
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
