import React from 'react';
import { connect } from 'react-redux';

class TodoListItem extends React.Component {



  render(){

    const {key, title} = this.props;

    return (
      <li key={key}>
        <p>{title}</p>
        <button onClick={() => this.props.removeFromList(title)}>Delete</button>
      </li>
    );

  };
}

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) => ({
  removeFromList: ( title ) => {
    dispatch({ type: 'REMOVE_LIST_ITEM', payload: { title }})
  }
});

export default connect(null, mapDispatchToProps)(TodoListItem);
