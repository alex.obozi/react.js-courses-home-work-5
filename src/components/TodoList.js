import React from 'react';
import {connect} from "react-redux";

class TodoList extends React.Component {

  removeItem = (id) => {
    this.props.removeFromList( id );
  };

  toggleDone = (id) => {
    const changedDoneList = this.props.list.map( item => {
      if(item.id === id){
        item.done = !item.done;
        return item;
      }
      return item;
    });
    this.props.toggleDone(changedDoneList);
  };

  checkLoc = () => {
    let {slug} = this.props.match.params;
    if(slug === 'done'){
      return this.props.list.filter(item => item.done === true)
    }else if(slug === 'undone'){
      return this.props.list.filter(item => item.done === false)
    }else {
      return this.props.list
    }
  };

  render(){

    const {removeItem, toggleDone, checkLoc} = this;

    return (
      <ul>
      {
        checkLoc().map(item =>
          <li key={item.id} >
            <p
              className={item.done ? 'done' : ''}
              onClick={() => toggleDone(item.id)}
            >
              {item.title}
            </p>
            <button onClick={() => removeItem(item.id)}>Delete</button>
          </li>
        )
      }
      </ul>
    );
  }
}

const mapStateToProps = ( state ) => ({
  list: state.todoList
});

const mapDispatchToProps = ( dispatch ) => ({
  removeFromList: ( id ) => {
    dispatch({ type: 'REMOVE_LIST_ITEM', payload: id})
  },
  toggleDone: ( changedDoneList ) => {
    dispatch({ type: 'TOGGLE_LIST_ITEM_DONE', payload: changedDoneList})
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
