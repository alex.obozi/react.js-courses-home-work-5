const initialState = {
  inputValue: '',
  todoList: [{
    id: 0,
    title: 'hello',
    done: false
  }],
  filteredList: []
};

const reducer = (state = initialState, action) => {

  switch ( action.type ) {
    case 'ADD_LIST_ITEM':
      return {
        ...state,
        todoList: [...state.todoList, action.payload]
      };
    case 'REMOVE_LIST_ITEM':
      return {
        ...state,
        todoList: state.todoList.filter(item => item.id !== action.payload)
      };
    case 'TOGGLE_LIST_ITEM_DONE':
      return {
        ...state,
        todoList: action.payload
      };

    default:
        return state;
  }
};

export default reducer;
